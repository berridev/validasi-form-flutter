import 'package:flutter/material.dart';

class AccountForm extends StatefulWidget {
  @override
  _AccountFormState createState() => _AccountFormState();
}

class _AccountFormState extends State<AccountForm> {
  final _formKey = GlobalKey<FormState>();
  String _name = "";

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      autovalidate: true,
      child: Column(
        children: <Widget>[
          TextFormField(
            onSaved: (String val) => setState(() => _name = val),
            validator: (value) {
              if (value.isEmpty) {
                return "Nama Harus Di Isi";
              }
              if (value.length < 2) {
                return "Nama Harus Lebih dari 2 Karakter";
              }
            },
            decoration: InputDecoration(
                labelText: "Nama",
                // hintText: "cth: Berri Pbrimaputra",
                helperText: "Tulis Nama Lengkap Anda"),
          ),
          RaisedButton(
            child: Text("Kirim"),
            onPressed: () {
              _formKey.currentState.save();
              _formKey.currentState.validate()
                  ? Scaffold.of(context).showSnackBar(
                      SnackBar(
                        content: Text("Terkirim"),
                      ),
                    )
                  : Scaffold.of(context).showSnackBar(
                      SnackBar(
                        backgroundColor: Colors.red,
                        content: Text("Gagal Terkirim"),
                      ),
                    );
            },
          ),
          Text(_name)
        ],
      ),
    );
  }
}
